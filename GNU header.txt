Copyright © 2011 Vincent Le Gallic

This file is part of Note Kfet 2015.

Note Kfet 2015 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Note Kfet 2015 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Note Kfet 2015.  If not, see <http://www.gnu.org/licenses/>.



The copy of GNU General Public License is in the main folder,
in the file "GNU General Public License.txt".
For French (unofficial) translation,
see <http://www.fsffrance.org/gpl/gpl-fr.fr.html>

Une copie de la License Publique Générale GNU est disponible dans le dossier
principal, dans le fichier "GNU General Public License.txt".
Pour une traduction (non-officielle) en français,
voir <http://www.fsffrance.org/gpl/gpl-fr.fr.html>
